from sympy import factor, expand, Symbol, I, re, degree
from mpmath import mp
import math

#function that returns a symbolic version of the cyclotomic polynomial
#input: x = symbolic variable x=Symbol('x',real=True)
#input: n = order of the desired cyclotomic Polynomial
#output: cyclotomic = symbolic cyclotomic polynomial of x of order n in sum form
def get_cyclotomic(n, x):
	#first build the cyclotomic polynomial in product form
	factored=1#factored cyclotomic polynomial will be built here
	for i in range(n-1):
		i+=1#we don't want 0 to n-2, we want 1 to n-1
		if math.gcd(i,n) == 1:#check for coprime, bc the product is iterated over the coprimes
			factored = factored * (x-mp.exp(2*I*mp.pi*i/n))
	cyclotomic_dirty = re(expand(factored))#unfactored version of the cyclotomic polynomial

	#now turn all the coefficients into integers not floats so we can subtract them properly
	order = degree(cyclotomic_dirty,x)#order of the polynomial
	#round all the coefficients to integers
	coefficients = [1]+[(round(cyclotomic_dirty.coeff(pow(x,i+1)))) for i in range(order)]#array consisting of coefficients to cyclotomic polynomial.  coefficients[k] is coefficient for x^(k).  we prepend the 1 because the coeff fofr x^0 is always 1
	#now rebuild the cyclotomic Polynomial with the rounded coefficients
	cyclotomic = 0#build the clean polynomial here
	deg=0#counter for the degree of the term as we build the polynomial
	for coeff in coefficients:
		cyclotomic += coeff * (x**deg)
		deg += 1
	return(cyclotomic)


#get the coefficients of a Polynomial
#input: f= symbolic polynomial in one variable
#inputs:x = that variable of f
#output:tuple (coeffs, terms)
#output: coeffs = list with coeffs[k] = a_k and f(x) = \sum\limits_k a_k * x^k
#output: terms = dictionary with key is coeff value, value is list of terms that have that coefficient.
def get_coeffs(f,x):
	order = degree(f,x)#order of the polynomial
	#print(order)
	coefficients = [1]+[(f.coeff(pow(x,i+1))) for i in range(order)]#array consisting of coefficients to cyclotomic polynomial.  coefficients[k] is coefficient for x^(k).  we prepend the 1 because the coeff fofr x^0 is always 1
	#print(coefficients)


	#get the terms that have each coefficient
	#dictionary key is coeff value, value is list of terms that have that coefficient
	terms = {
	 0: [i for i in range(len(coefficients)) if coefficients[i] ==  0],
	 1: [i for i in range(len(coefficients)) if coefficients[i] ==  1],
	 -1:[i for i in range(len(coefficients)) if coefficients[i] == -1],
	 2: [i for i in range(len(coefficients)) if coefficients[i] ==  2],
	 -2:[i for i in range(len(coefficients)) if coefficients[i] == -2],
	}
	return(coefficients, terms)



#function that returns which terms have which coefficient vaues for a cyclotomic polynomial of the given order
#input: order of the cyclotomic polynomial.
#output: dictionary key is coeff value, value is list of terms that have that coefficient
def get_cyclotomic_coeffs(n):
	x=Symbol('x',real=True)

	(coeffs,terms) = get_coeffs(get_cyclotomic(n,x),x)
	return(terms)

#function for getting a symbolic polynomial for a given fiber
#input: x=variable:x=Symbol('x',real=True)
#input: n = order of the cyclotomic polynomial the fiber is inside
#input: p = prime factor of n - we are gettig a p-fiber
#iput: index = any term in the fiber - usually put the lowest term in the fiber
#output: f = symbolic polynomial \sum\limits_k=0^{p-1}x^{k\frac{n}{p}+index}
def get_fiber(x, n, p, index):
	f=0#this is the fiber, we will build it in the loop
	for k in range(p):
		current_order = int((k*n/p+index) % n) #if index is the lowest term then this modulo operator will never be needed
		f += pow(x, current_order)
	return(f)








def test():

	#x=Symbol('x',real=True)
	#n=15
	#p=3
	#index=5
	#fiber = get_fiber(x,n,p,index)
	#print(fiber)
	#(a,b) = get_coeffs(fiber,x)
	#(a,b) = get_coeffs(get_cyclotomic(n,x),x)
	#print(a)
	#print(b)

	p=3
	x=Symbol('x',real=True)
	m=2
	q = 6*m-1
	r = 6*m+1
	n=p*q*r

	# s_data is dict.  key is fiber index, value is coeff (should just be +1 or -1)
	#p_data = {0:1,3:1}
	#q_data = {1:-1}
	p_data = {}
	q_data = {}
	r_data = {}

	#restructure this into a dictionary with the starting spot and the size of the 'block' as the key and value
	#q_starts holds a list of where the m sized blocks of fibers start to be used like this: q_data[(k+j)*int(n/q)] = 1 where k goes from 0 to m-1 and j is from q_starts
	q_starts = [0, m*int(n/r), (4*m+1)*int(n/r), (5*m+1)*int(n/r), q*r, 2*q*r+(5*m+1)*int(n/r)]
	r_starts = [q*r+(2*m)*int(n/q),q*r*2+m*int(n/q)]#just for the size m blocks
	p_starts = [m*int(n/q), 3*m*int(n/q), 2*m*int(n/q)+m*int(n/r), 3*m*int(n/q)+m*int(n/r),
				(4*m+1)*int(n/r), (5*m+1)*int(n/r), (4*m+1)*int(n/r)+m*int(n/q), (5*m+1)*int(n/r)+2*m*int(n/q)]
	p_neg2_starts = [2*m*int(n/q), (5*m+1)*int(n/r)+1*m*int(n/q)]#these have value -2
	#fill in the s_data using s_starts
	for k in range(m):#inside this loop, create all the 'blocks of fibers' of width m
		#create the q-fibers
		for j in q_starts:
			q_data[j+(k*int(n/r))] = 1
		for j in r_starts:
			r_data[j+(k*int(n/q))] = 1
		for t in range(m):#becasue we are making m by m rectangles
			for j in p_starts:
				p_data[j + (k*int(n/r)+t*int(n/q))] = -1
			for j in p_neg2_starts:
				p_data[j + (k*int(n/r)+t*int(n/q))] = -2


	for k in range(2*m-1):#inside this loop, create all the 'blocks of fibers' of width 2m-1
		r_data[(k+4*m)*int(n/q)] = -1 #only block of size not m


	f = 0#function made up of these fibers.  build it here.
	for index in p_data:
		f += get_fiber(x,n,p,index) * p_data[index]
	for index in q_data:
		f += get_fiber(x,n,q,index)* q_data[index]
	for index in r_data:
		f += get_fiber(x,n,r,index)* r_data[index]

	cyclotomic = get_cyclotomic(n, x)
	(coeffs, terms) = get_coeffs(cyclotomic,x)
	print(coeffs)
	print(terms)
	print("\n")

	difference = cyclotomic - f

	print(cyclotomic)
	print("\n")
	print(f)
	print("\n")
	print(difference)




	(coeffs, terms) = get_coeffs(f,x)
	neg = terms[-1]
	pos = terms[1]
	neg2 = terms[-2]
	neg_list_str = "["
	for i in neg:
	    neg_list_str += str(i)+" "
	neg_list_str +="]"
	#then the same for positive (this will want to be a function)
	pos_list_str = "["
	for i in pos:
	    pos_list_str += str(i)+" "
	pos_list_str +="]"
	#and for -2
	neg2_list_str = "["
	for i in neg2:
	    neg2_list_str += str(i)+" "
	neg2_list_str +="]"
	#add the lines of the postscript one at a time
	cell_size = 25
	stylemode = 0
	new_commands = "<< /PageSize ["+str(p*(r+1)*cell_size)+" "+str((q+1)*cell_size)+"] >> setpagedevice\n"#this should not be static
	new_commands += "/Times-Roman findfont\n08 scalefont\n"
	new_commands += "/cell "+str(cell_size)+" def\n"
	new_commands +="cell 2 div\ncell 2 div\ntranslate\n"
	new_commands += "/style "+str(stylemode)+" def\n"
	new_commands +="/p "+str(p)+" def\n"
	new_commands +="/q "+str(q)+" def\n"
	new_commands +="/r "+str(r)+" def\n"
	new_commands +="/negative "+neg_list_str+" def\n"
	new_commands +="/positive "+pos_list_str+" def\n"
	new_commands +="/neg2 "+neg2_list_str+" def\n"
	new_commands += "newpath\np q r cell positive negative neg2 style lll3\nstroke"




	#write output to file
	#first copy over the functions we will use, then add the lines for this specific lll_diagram
	function_file = "lll_diagrams.ps"#where our plotting functions already
	output_file = "out3.ps"#will contain functions and our new commands
	functions = ""#will contain contents of function output_file
	with open(function_file, 'r') as function_handle:
	    functions = function_handle.read()
	with open(output_file, "w+") as out_handle:
	    out_handle.write(functions)
	    out_handle.write("\n%New commands generated by python\n")
	    out_handle.write(new_commands)
	print("PS file generation complete: "+output_file)







#test()
